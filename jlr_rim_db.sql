

/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`rim_jlr` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `rim_jlr`;

/*Table structure for table `jlr_order_acknowledgement` */

DROP TABLE IF EXISTS `jlr_order_acknowledgement`;

CREATE TABLE `jlr_order_acknowledgement` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `FileName` varchar(20) NOT NULL,
  `Order_Date` date NOT NULL,
  `RIM_Order_No` varchar(9) NOT NULL,
  `RIM_Order_Ref` varchar(7) NOT NULL,
  `RIM_Order_Account` varchar(7) NOT NULL,
  `Client` varchar(24) NOT NULL,
  `Part_No` varchar(24) NOT NULL,
  `Quantity` int(6) NOT NULL,
  `DateReceived` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=latin1;

/*Table structure for table `jlr_server` */

DROP TABLE IF EXISTS `jlr_server`;

CREATE TABLE `jlr_server` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PartsDept_ID` int(11) NOT NULL,
  `RIM_URL` varchar(250) NOT NULL,
  `Username` varchar(20) NOT NULL,
  `Password` varchar(20) NOT NULL,
  `Last_Run_Time` datetime DEFAULT NULL,
  `Active` tinyint(1) NOT NULL,
  `UniPartAccountRef` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Table structure for table `log_type` */

DROP TABLE IF EXISTS `log_type`;

CREATE TABLE `log_type` (
  `LogType` int(1) NOT NULL,
  `Description` varchar(100) NOT NULL,
  PRIMARY KEY (`LogType`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `part_movements` */

DROP TABLE IF EXISTS `part_movements`;

CREATE TABLE `part_movements` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PartNo` varchar(24) NOT NULL,
  `MoveType` varchar(1) NOT NULL,
  `MoveDirn` varchar(1) DEFAULT NULL,
  `MoveQty` int(4) NOT NULL,
  `MoveDate` date NOT NULL,
  `MoveTime` time NOT NULL,
  `StUseInd` varchar(8) NOT NULL,
  `StorKey` varchar(9) DEFAULT NULL,
  `QtyAfter` int(5) NOT NULL,
  `QtySign` varchar(1) DEFAULT NULL,
  `TransactionDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=662 DEFAULT CHARSET=latin1;

/*Table structure for table `rim_error_log` */

DROP TABLE IF EXISTS `rim_error_log`;

CREATE TABLE `rim_error_log` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `Transaction_Type` int(1) NOT NULL,
  `Error_Details` varchar(500) DEFAULT NULL,
  `Date_Added` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=latin1;

/*Table structure for table `rim_frequency` */

DROP TABLE IF EXISTS `rim_frequency`;

CREATE TABLE `rim_frequency` (
  `FreequencyID` int(4) NOT NULL,
  `FrequencyType` int(2) NOT NULL DEFAULT '-1',
  `Frequency` int(4) NOT NULL DEFAULT '0',
  `Description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`FreequencyID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `rim_log` */

DROP TABLE IF EXISTS `rim_log`;

CREATE TABLE `rim_log` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `TransmissionType` int(1) NOT NULL,
  `FileName` varchar(20) NOT NULL,
  `DateSent` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=latin1;

/*Table structure for table `rim_schedules` */

DROP TABLE IF EXISTS `rim_schedules`;

CREATE TABLE `rim_schedules` (
  `ID` int(4) NOT NULL,
  `TransmissionID` int(11) NOT NULL,
  `InitialRuntime` datetime DEFAULT NULL,
  `LastRunTime` datetime DEFAULT NULL,
  `Active` smallint(1) NOT NULL,
  `PartsDeptID` int(10) NOT NULL,
  `FrequencyID` int(10) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `rim_transmissions` */

DROP TABLE IF EXISTS `rim_transmissions`;

CREATE TABLE `rim_transmissions` (
  `ID` int(10) NOT NULL,
  `FileName` varchar(70) NOT NULL,
  `Description` varchar(250) DEFAULT NULL,
  `Last_ID` int(8) unsigned zerofill DEFAULT '00000000',
  `Last_Sent` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/* Procedure structure for procedure `GetDailyStock` */

/*!50003 DROP PROCEDURE IF EXISTS  `GetDailyStock` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `GetDailyStock`(IN deptID int)
BEGIN
SELECT 
CONCAT(CASE WHEN deptID = 521 THEN '02' WHEN deptID = 541 THEN '28' ELSE '00' END, sm.part_number) AS 'prodkey', 
(CASE WHEN sm.stock_type = 'S' THEN 'N' ELSE 'Y' END) AS 'Non_Stock_Item',DATE_FORMAT(sm.last_sales_date, '%d%m%y') as last_sales_date, sm.on_hand, 
(CASE WHEN sm.on_hand >= 0 THEN '+' ELSE '-' END) AS 'quantity_sign',sm.min_order_qty,sm.DIRECTORY,sm.retail_price, ' ' AS 'retailer_analysis_code',
sm.stock_category
FROM replication.stock_master sm
inner join central.department_workspace dw on dw.stock_dir = sm.directory
INNER JOIN central.department d ON d.workspace_code = dw.code
WHERE d.code = deptID ORDER BY part_number ;
END */$$
DELIMITER ;

/* Procedure structure for procedure `GetStock` */

/*!50003 DROP PROCEDURE IF EXISTS  `GetStock` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `GetStock`()
BEGIN
SELECT part_number, 'Non Stock Y/N', on_hand, (CASE WHEN on_hand < 0 THEN '-' ELSE '+' END) AS 'Stock_Quality_Sign', min_order_qty,DIRECTORY,retail_price, ' ' AS 'retailer_analysis_code',stock_category, '02N0311' as 'Storkey'
FROM replication.stock_master WHERE DIRECTORY IN('stockja','stockcm');
END */$$
DELIMITER ;

/* Procedure structure for procedure `GetStockMovement` */

/*!50003 DROP PROCEDURE IF EXISTS  `GetStockMovement` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `GetStockMovement`(in lastRun DATETIME, in deptID int)
BEGIN
SELECT DISTINCT CONCAT((CASE WHEN pm.parts_movement_dir = 'stockja' THEN '02' WHEN pm.parts_movement_dir = 'stockcm' THEN '28' 
ELSE '02' END), pm.parts_movement_part) 
AS 'prodkey', 
(CASE WHEN parts_movement_type = 1 THEN 'S'
WHEN parts_movement_type = 2 THEN 'R'
WHEN parts_movement_type = 3 THEN 'A'
WHEN parts_movement_type = 4 THEN 'D'
WHEN parts_movement_type = 5 THEN 'D'
WHEN parts_movement_type = 6 THEN 'A'
WHEN parts_movement_type = 7 THEN 'L'
WHEN parts_movement_type = 8 THEN 'S'
WHEN parts_movement_type = 9 THEN 'S'
WHEN parts_movement_type = 10 THEN 'A'
WHEN parts_movement_type = 11 THEN 'L'
WHEN parts_movement_type = 12 THEN 'A'
WHEN parts_movement_type = 13 THEN 'A'
WHEN parts_movement_type = 14 THEN 'A'
WHEN parts_movement_type = 15 THEN 'A'
ELSE parts_movement_type
END) AS movetype,
(CASE WHEN pm.parts_movement_qty < 0 or parts_movement_type in (1,8) THEN '-' ELSE '+' END) AS 'moveDirn', pm.parts_movement_qty, 
date_format(pm.parts_movement_date, '%d%m%y') as moveDate, date_format(curdate(), '%H%i') AS 'moveTime', pm.parts_movement_dir, '02N0311' AS 'storKey', pm.parts_movement_bal AS 'qtyAfter',
(CASE WHEN pm.parts_movement_bal < 0 THEN '-' ELSE '+' END) AS 'qtysign',
coalesce(ack.RIM_Order_No,(CASE WHEN parts_movement_type IN (4,5) THEN 'O' ELSE '' END)) AS OrderNo
FROM replication.parts_movement pm
INNER JOIN replication.stock_master sm ON sm.part_number = pm.parts_movement_part AND sm.directory = pm.parts_movement_dir
INNER JOIN central.department_workspace dw ON dw.stock_dir = sm.directory
INNER JOIN central.department d ON d.workspace_code = dw.code
left join jlr_order_acknowledgement as ack on ack.Rim_Order_ref = pm.RIM_order_Ref and ack.part_no = pm.parts_movement_part
#WHERE pm.parts_movement_date > lastRun AND parts_movement_type IN(7)
WHERE pm.parts_movement_date > lastRun AND parts_movement_type NOT IN('I','M','S','B')
and d.code = deptID;
END */$$
DELIMITER ;

/* Procedure structure for procedure `List_InitialLoadAllParts_notusing` */

/*!50003 DROP PROCEDURE IF EXISTS  `List_InitialLoadAllParts_notusing` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `List_InitialLoadAllParts_notusing`(PartsDeptID int)
BEGIN
    
    declare FranchiseCode varchar(5);
    declare stock_dir varchar(30);
    CREATE TEMPORARY TABLE IF NOT EXISTS 
  TempAllPats ( INDEX(part_number) ) 
ENGINE=MyISAM 
AS (
	select part_number from (SELECT tl.part_number as part_number
		FROM replication.transaction_log AS tl 
		INNER JOIN central.department AS dep ON dep.code = tl.department_id
		INNER JOIN central.department_workspace AS wp ON wp.code = dep.workspace_code
	WHERE tl.`date` > SUBDATE(CURDATE(),INTERVAL 12 MONTH)
	AND tl.department_id = PartsDeptID 
	AND  tl.franchise_code = dep.Default_Franchise_Code 
   
	UNION 
			
	SELECT 	ibjp.Part_Number as part_number
		FROM automate.invoice_booking_job_parts ibjp
		INNER JOIN automate.invoice_booking_job_parts_account ibjpa ON ibjpa.booking_job_parts_id = ibjp.ID AND ibjp.booked_on = 1
		INNER JOIN automate.invoice_booking_accounts iba ON iba.ID = ibjpa.booking_account_id
		INNER JOIN automate.monetary_transaction mt ON mt.ID = iba.monetary_transaction_id
		INNER JOIN automate.invoice_credit_transaction ict ON ict.monetary_transaction_id = mt.id
		#INNER JOIN automate.workshop_booking_record wbr ON wbr.booking_id = mt.record_id
		INNER JOIN central.department AS d ON d.code = ibjp.Department_ID
	WHERE ibjp.Department_ID = PartsDeptID AND ict.created_date > SUBDATE(CURDATE(),INTERVAL 12 MONTH)
	AND CASE WHEN d.Default_Franchise_Code = 'LR' THEN ibjp.Manufacturer_ID  = 34 ELSE ibjp.Manufacturer_ID  = 329 END ) as pt
	group by pt.part_number
     );
 
 -- select count(*) from TempAllPats;
 
  
	SELECT d.Default_Franchise_Code ,wp.stock_dir into FranchiseCode,stock_dir FROM central.department as d
	INNER JOIN central.department_workspace AS wp ON wp.code = d.workspace_code
	WHERE d.CODE = PartsDeptID;
  
  select stock_dir;
  select FranchiseCode;
  
  select * from TempAllPats;
        
     SELECT 
		coalesce(sm.min_order_qty,1) as MinStock,
		coalesce(sm.max_order_qty,9999)  AS MaxStock,
		COALESCE(sm.introduction_date,SUBDATE(CURDATE(),INTERVAL 1 YEAR)) AS StkIntrodDate,
		pt.part_number,
		FranchiseCode,
		PartsDeptID
	FROM TempAllPats as pt
	left join replication.stock_master AS sm 
	ON sm.part_number = pt.part_number AND 
	sm.franchise_code =  FranchiseCode
	AND sm.directory = stock_dir group by pt.part_number;
	
	drop temporary table TempAllPats;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `List_InitialLoastSalesFeed_Data` */

/*!50003 DROP PROCEDURE IF EXISTS  `List_InitialLoastSalesFeed_Data` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `List_InitialLoastSalesFeed_Data`(intPartsDeptID int)
BEGIN
    
   
	-- SELECT TotalMovementWorkshop;
	
	SELECT department_id,
	ClientNo,
	part_number,
	description,
	SUM(TotalQty)AS TotalQty,
	SUM(TotalPrice) AS TotalPrice,
	coalesce(prts.MinStock,1) AS MinStock,
	coalesce(prts.MaxStock,9999) AS MaxStock,
	coalesce(prts.StkIntrodDate,subdate(curdate(),interval 1 year)) AS StkIntrodDate,
	-- SUBDATE(CURDATE(), INTERVAL 1 YEAR) AS StkIntrodDate,
	MonthYear
	-- SUM(TotalMovement) AS TotalMovement
	 FROM (
	SELECT department_id,
	CASE WHEN dep.default_Franchise_code = 'LR'  THEN '28'	ELSE '02'  END AS ClientNo,
	tl.part_number,
	tl.description,
	SUM(tl.quantity) AS TotalQty,
	SUM(tl.net_total) AS TotalPrice,
	sm.min_order_qty AS MinStock,
	sm.max_order_qty AS MaxStock,
	sm.introduction_date AS StkIntrodDate,
	MONTH(tl.`date`) AS sales_month,
	YEAR(tl.`date`) AS sales_year,
	CONCAT(LPAD(MONTH(tl.`date`),2,'0'),'-',YEAR(tl.`date`)) AS MonthYear
	-- TotalMovementCounter AS TotalMovement
	
	FROM replication.transaction_log AS tl 
	INNER JOIN central.department AS dep ON dep.code = tl.department_id
	LEFT JOIN (SELECT 
			sm.min_order_qty,
			sm.max_order_qty,
			sm.introduction_date,
			sm.part_number,
			sm.franchise_code,
			sm.bin_location
		FROM replication.stock_master AS sm GROUP BY sm.part_number)AS sm
		 ON sm.part_number = tl.part_number AND 
		sm.franchise_code = tl.franchise_code AND sm.bin_location = tl.bin_location 
	WHERE tl.`date` > SUBDATE(CURDATE(),INTERVAL 11 MONTH)
	AND tl.department_id = intPartsDeptID AND tl.is_credit =1
	AND tl.franchise_code = dep.Default_Franchise_Code
	-- AND CONCAT('02',tl.part_number)  = '02AJ8006484'
	GROUP BY MonthYear,tl.part_number
	
	UNION ALL 
  
	SELECT ibjp.Department_ID,
	
		CASE WHEN d.default_Franchise_code = 'LR'  THEN '28' ELSE '02'  END AS ClientNo,
		ibjp.part_number,
		ibjp.Description,
		SUM(ibjp.Quantity) AS TotalQty,
		SUM(ibjp.Nett_Price) AS NetPrice,
		sm.min_order_qty AS MinStock,
		sm.max_order_qty AS MaxStock,
		sm.introduction_date AS StkIntrodDate,
		MONTH(ict.Created_Date) AS sales_month,
		YEAR(ict.Created_Date) AS sales_year,
		CONCAT(LPAD(MONTH(ict.Created_Date),2,'0'),'-',YEAR(ict.Created_Date)) AS MonthYear
		-- TotalMovementWorkshop AS TotalMovement
		
	FROM automate.invoice_booking_job_parts ibjp
	INNER JOIN automate.invoice_booking_job_parts_account ibjpa ON ibjpa.booking_job_parts_id = ibjp.ID AND ibjp.booked_on = 1
	INNER JOIN automate.invoice_booking_accounts iba ON iba.ID = ibjpa.booking_account_id
	INNER JOIN automate.monetary_transaction mt ON mt.ID = iba.monetary_transaction_id
	INNER JOIN automate.invoice_credit_transaction ict ON ict.monetary_transaction_id = mt.id
	-- INNER JOIN automate.workshop_booking_record wbr ON wbr.booking_id = mt.record_id
	INNER JOIN central.department AS d ON d.code = ibjp.Department_ID
	INNER JOIN central.department_workspace AS wp ON wp.code = d.workspace_code
	LEFT JOIN (SELECT 
			sm.min_order_qty,
			sm.max_order_qty,
			sm.introduction_date,
			sm.part_number,
			sm.franchise_code,
			sm.bin_location
		FROM replication.stock_master AS sm GROUP BY sm.part_number)AS sm
		 ON sm.part_number = ibjp.part_number AND 
		sm.franchise_code = d.default_Franchise_code AND sm.bin_location = wp.stock_dir 
	WHERE ibjp.Department_ID = intPartsDeptID AND ict.created_date > SUBDATE(CURDATE(),INTERVAL 11 MONTH)
	AND  (ict.IsCredit = 1 OR (ict.IsCredit = 0 AND ibjp.booked_on = 0)) 
	AND CASE WHEN d.Default_Franchise_Code = 'LR' THEN ibjp.Manufacturer_ID  = 34 ELSE ibjp.Manufacturer_ID  = 329 END
	--  AND CONCAT('02',ibjp.part_number) = '02AJ8006484'
	GROUP BY MonthYear,part_number) AS prts
	GROUP BY MonthYear,part_number;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `List_InitialSalesFeed_Data` */

/*!50003 DROP PROCEDURE IF EXISTS  `List_InitialSalesFeed_Data` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `List_InitialSalesFeed_Data`(intPartsDeptID int)
BEGIN
			
		
	-- select TotalMovementWorkshop;
	
	select department_id,
	ClientNo,
	part_number,
	description,
	sum(TotalQty)as TotalQty,
	sum(TotalPrice) as TotalPrice,
	MinStock,
	MaxStock,
	subdate(Curdate(), interval 1 year) as StkIntrodDate,
	MonthYear
	-- sum(TotalMovement) as TotalMovement
	 from (
	SELECT department_id,
	CASE WHEN dep.default_Franchise_code = 'LR'  THEN '28'	ELSE '02'  END AS ClientNo,
	tl.part_number,
	tl.description,
	sum(tl.quantity) as TotalQty,
	sum(tl.net_total) as TotalPrice,
	coalesce(sm.min_order_qty,1) AS MinStock,
	coalesce(sm.max_order_qty,9999) AS MaxStock,
	COALESCE(sm.introduction_date,SUBDATE(CURDATE(),INTERVAL 1 YEAR))as StkIntrodDate,
	month(tl.`date`) as sales_month,
	year(tl.`date`) as sales_year,
	concat(lpad(month(tl.`date`),2,'0'),'-',year(tl.`date`)) as MonthYear
	-- TotalMovementCounter as TotalMovement
	
	FROM replication.transaction_log as tl 
	inner join central.department as dep on dep.code = tl.department_id
	LEFT JOIN (SELECT 
			sm.min_order_qty,
			sm.max_order_qty,
			sm.introduction_date,
			sm.part_number,
			sm.franchise_code,
			sm.bin_location
		FROM replication.stock_master AS sm GROUP BY sm.part_number)AS sm
		 ON sm.part_number = tl.part_number AND 
		sm.franchise_code = tl.franchise_code AND sm.bin_location = tl.bin_location 
	WHERE tl.`date` > SUBDATE(CURDATE(),INTERVAL 11 MONTH)
	and tl.department_id = intPartsDeptID and tl.is_credit =0
	AND tl.franchise_code = dep.Default_Franchise_Code
  -- and CONCAT('02',tl.part_number)  = '02AJ8006484'
	group by MonthYear,part_number
	
	union all 
  
	SELECT ibjp.Department_ID,
		CASE WHEN d.default_Franchise_code = 'LR'  THEN '28' ELSE '02'  END AS ClientNo,
		ibjp.part_number,
		ibjp.Description,
		SUM(ibjp.Quantity) AS TotalQty,
		SUM(ibjp.Nett_Price) AS NetPrice,
		coalesce(sm.min_order_qty,1) AS MinStock,
		coalesce(sm.max_order_qty,9999) AS MaxStock,
		coalesce(sm.introduction_date,subdate(curdate(),interval 1 year)) AS StkIntrodDate,
		MONTH(ict.Created_Date) AS sales_month,
		YEAR(ict.Created_Date) AS sales_year,
		cONCAT(LPAD(MONTH(ict.Created_Date),2,'0'),'-',YEAR(ict.Created_Date)) AS MonthYear
		-- TotalMovementWorkshop as TotalMovement
	FROM automate.invoice_booking_job_parts ibjp
	INNER JOIN automate.invoice_booking_job_parts_account ibjpa ON ibjpa.booking_job_parts_id = ibjp.ID AND ibjp.booked_on = 1
	INNER JOIN automate.invoice_booking_accounts iba ON iba.ID = ibjpa.booking_account_id
	INNER JOIN automate.monetary_transaction mt ON mt.ID = iba.monetary_transaction_id
	INNER JOIN automate.invoice_credit_transaction ict ON ict.monetary_transaction_id = mt.id
	#INNER JOIN automate.workshop_booking_record wbr ON wbr.booking_id = mt.record_id
	INNER JOIN central.department AS d ON d.code = ibjp.Department_ID
	INNER JOIN central.department_workspace AS wp ON wp.code = d.workspace_code
	LEFT JOIN (SELECT 
			sm.min_order_qty,
			sm.max_order_qty,
			sm.introduction_date,
			sm.part_number,
			sm.franchise_code,
			sm.bin_location
		FROM replication.stock_master AS sm GROUP BY sm.part_number)AS sm
		 ON sm.part_number = ibjp.part_number AND 
		sm.franchise_code = d.default_Franchise_code AND sm.bin_location = wp.stock_dir 
	WHERE ibjp.Department_ID = intPartsDeptID AND ict.created_date > SUBDATE(CURDATE(),INTERVAL 11 MONTH)
	AND ibjp.booked_on = 1 AND ict.IsCredit = 0
	and CASE WHEN d.Default_Franchise_Code = 'LR' THEN ibjp.Manufacturer_ID  = 34 ELSE ibjp.Manufacturer_ID  = 329 END
	--   AND CONCAT('02',ibjp.part_number)  = '02AJ8006484'
	group by MonthYear,part_number) as prts
	group by MonthYear,part_number;
	
 
	
	
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `List_RIM_ServerDetails` */

/*!50003 DROP PROCEDURE IF EXISTS  `List_RIM_ServerDetails` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `List_RIM_ServerDetails`()
BEGIN
	
	select 	sc.ID,
		sc.InitialRuntime,
		sc.LastRunTime,
		case when (fr.FrequencyType = 0 and sc.LastRunTime is null) then subdate(now(),interval 1 minute) 
		 when fr.FrequencyType = 1 then  adddate(NOW(),INTERVAL fr.Frequency day) 
		 when fr.FrequencyType = 2 THEN  ADDDATE(NOW(),INTERVAL fr.Frequency Hour)
		end as NextRunTime,
		sc.PartsDeptID,
		tr.ID as TransmissionID,
		tr.FileName,
		sr.RIM_URL,
		sr.UserName,
		sr.`Password`,
		fr.FrequencyType,
		fr.Frequency,
		fr.Description as FrequencyDesc,
		sr.UnipartAccountRef,
		d.Default_Franchise_Code as FranchiseCode
	from rim_schedules as sc
	inner join rim_transmissions as tr on tr.ID = sc.TransmissionID
	inner join rim_frequency as fr on fr.FreequencyID = sc.FrequencyID
	inner join jlr_server as sr on sr.PartsDept_ID = sc.PartsDeptID
	INNER JOIN central.department AS d ON d.code = sr.PartsDept_ID
	where sr.Active = 1 and sc.Active = 1
	order by sc.PartsDeptID,sc.ID;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `LogError` */

/*!50003 DROP PROCEDURE IF EXISTS  `LogError` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `LogError`(IN pTranType INT(1), in pDetails varchar(500))
BEGIN
	insert into rim_error_log(Transaction_Type, Error_Details, Date_Added) 
	values (pTranType, pDetails, now());
END */$$
DELIMITER ;

/* Procedure structure for procedure `Log_RIM_File` */

/*!50003 DROP PROCEDURE IF EXISTS  `Log_RIM_File` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `Log_RIM_File`(in tType int(1), in fileName varchar(20), in dateSent datetime)
BEGIN
insert into rim_log (TransmissionType, FileName, DateSent)
values (tType, fileName, dateSent);
UPDATE rim_transmissions SET Last_ID = Last_ID + 1, Last_Sent = NOW() WHERE ID = tType;
END */$$
DELIMITER ;

/* Procedure structure for procedure `UpdateOrder` */

/*!50003 DROP PROCEDURE IF EXISTS  `UpdateOrder` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `UpdateOrder`(IN pFile varchar(20), in pDate datetime, in pOrder varchar(9), in pRef varchar(7), in pAccount varchar(7), in pClient varchar(24), in pPartNo varchar(24), in pQuantity int(6))
BEGIN
insert into jlr_order_acknowledgement(FileName, Order_Date, RIM_Order_No, RIM_Order_Ref, RIM_Order_Account, Client, Part_No, Quantity)
values (pFile, pDate, pOrder, pRef, pAccount, pClient, pPartNo, pQuantity);
END */$$
DELIMITER ;

/* Procedure structure for procedure `UpdateStockMovement` */

/*!50003 DROP PROCEDURE IF EXISTS  `UpdateStockMovement` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `UpdateStockMovement`(in pPartNo varchar(24), in pMove varchar(1), in pQty int(4), in pMoveDate int(8),
in pMoveTime int(4), in pUseInd varchar(8), in pStorKey varchar(9), in pQtyAfter int(5))
BEGIN
insert into part_movements(PartNo, MoveType, MoveQty, MoveDate, MoveTime, StUseInd, StorKey, QtyAfter)
values (pPartNo, pMove, pQty,(str_to_date(pMoveDate, '%d%m%Y')), (str_to_date(pMoveTime, '%h%i')), pUseInd, pStorKey, pQtyAfter);
END */$$
DELIMITER ;

/* Procedure structure for procedure `Update_LastRunTime` */

/*!50003 DROP PROCEDURE IF EXISTS  `Update_LastRunTime` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `Update_LastRunTime`(IntTransmissionID int,intPartsDeptID int)
BEGIN
	UPDATE rim_schedules SET LastRunTime = NOW(),InitialRunTime = coalesce(InitialRunTime,now()) WHERE TransmissionID = IntTransmissionID AND Active = 1 AND PartsDeptID = intPartsDeptID;
    END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;